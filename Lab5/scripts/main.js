function DateTimeRenderInRealTime(parent, updateTime)
{
  if (parent != undefined)
  {
    DateTimeRender(parent);
    setInterval(() => DateTimeRender(parent), updateTime);
  }
}

function DateTimeRender(parent)
{
  if (parent != undefined)
  {
    var date = new Date();
    var days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thurdsday", "Friday", "Saturday"];
    var dateFormat = date.getHours() + "." + date.getMonth()
    + "." + (date.getFullYear() % 100) + " " + days[date.getDay()];
    parent.textContent = "Today is "+ dateFormat;
  }
}

function SetDocumentCookies()
{
  var documentName = GetDocumentName();

  var cookieSplit = document.cookie.split(";");
  var cookieNames = [];
  var cookieValues = [];

  var currentCookieName = documentName;
  var currentValueIndex;

  for (var i = 0; i < cookieSplit.length; i++)
  {
    var values = cookieSplit[i].split("=");
    cookieNames[i] = values[0].trim();
    cookieValues[i] = values[1];
    if (cookieNames[i] === currentCookieName) currentValueIndex = i;
  }
  var currentCount = (currentValueIndex != undefined) ? (cookieValues[currentValueIndex] - 0) + 1 : 1;

  var cookieExpires = new Date();
  cookieExpires.setDate(cookieExpires.getDate() + 10);

  document.cookie = currentCookieName + "=" + currentCount
                  + "; expires=" + cookieExpires.toUTCString() + ";";
}

function GetDocumentName()
{
  var documentLocation = document.location.href;
  var documentLocationSplit = documentLocation.split("/");
  var documentName = documentLocationSplit[documentLocationSplit.length - 1];
  return documentName;
}

function SetSessionStorage()
{
  var documentName = GetDocumentName();
  var currentStorageValue = sessionStorage.getItem(documentName);
  var currentValue = (currentStorageValue != undefined) ? currentStorageValue - 0 + 1 : 1;
  sessionStorage.setItem(documentName, currentValue);
}

function CreateAsideMenu()
{
  var aside = CreateAsideBlock();
  $(aside).attr("id", "sideNav");

  var ul = CreateUl();
  var names =
  [
    "Home",
    "History",
    "My Study",
    "Hobbies",
    "My Photo",
    "Contacts",
    "About"
  ];

  var links =
  [
    "index.html",
    "history.html",
    "study.html",
    "hobby.html",
    "gallery.html",
    "contacts.html",
    "about.html"
  ];
  for (var i = 0; i < links.length; i++)
  {
    var li = CreateLi();
    var a = document.createElement("a");

    $(a).text(names[i]);
    $(a).attr("href", links[i]);

    $(li).append(a);
    $(ul).append(li);
  }

  $(aside).append(ul);
  $("#root").prepend(aside);

  var animTime = 700;

  $(aside).on("mouseenter", function(event)
  {
    var content = $(event.currentTarget).children("ul");
    $(content).stop().animate({
      width: "100%",
      opacity: "1"
    }, animTime);
  })

  $(aside).on("mouseleave", function(event)
  {
    var content = $(event.currentTarget).children("ul");
    $(content).stop().delay(animTime * 2).animate({
      width: "0%",
      opacity: "0"
    }, animTime);
  })
}

function FormBoxCall(box, animTime)
{
  $(box).fadeIn(animTime);
}

$(document).ready(function()
{
  var dateBlock = document.querySelector("#header-logo .dateTime span");
  DateTimeRenderInRealTime(dateBlock, 1000);
  SetDocumentCookies();
  SetSessionStorage();
  CreateAsideMenu();

  var parent = $("#root");
  var box = CreateFormBox("Ты тыришь мои вкусняшки?");
  parent.append(box);

  $("#header-pivot-body").on("click", function()
  {
    FormBoxCall(box, 650);
  })
})

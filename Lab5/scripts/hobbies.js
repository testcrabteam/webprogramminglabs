// Use in page ONLY width
// render.js!!

function HobbyRender(parent, ...hobbyObj)
{
  if (parent != undefined && hobbyObj != undefined)
  {
    parent.textContent = "";
    HobbyComponentsRender(parent, ...hobbyObj);
  }
}

function Hobby(avatarPath, hobbyTextObj)
{
  this.avatarPath = avatarPath;
  this.hobbyText = hobbyTextObj;
}

function HobbyText(header, pArray)
{
  this.header = header;
  this.pArray = pArray;
}

function HobbyComponentsRender(parent, ...hobbyObj)
{
  for (var i = 0; i < hobbyObj.length; i++)
  {
    HobbyOneComponentRender(parent, hobbyObj[i]);
  }
}

function HobbyOneComponentRender(parent, element)
{
  var contentBlock = CreateDivWithClass("history");
  var contentBlockLimit = CreateContentBlockLimitWithAvatarAndContent(element);
  contentBlock.appendChild(contentBlockLimit);
  parent.appendChild(contentBlock);
}

function CreateContentBlockLimitWithAvatarAndContent(element)
{
  var elementText = element.hobbyText;
  var contentBlockLimit = CreateDivWithClass("limitation");
  var avatar = CreateAvatar(element.avatarPath);
  var content = CreateContentWidthHeaderAndText(elementText);
  contentBlockLimit.appendChild(avatar);
  contentBlockLimit.appendChild(content);
  return contentBlockLimit;
}

function CreateAvatar(avatarPath)
{
  var img = CreateDivWithClass("history-bg");
  img.style.backgroundImage = "url(" + avatarPath + ")";
  return img;
}

function CreateContentWidthHeaderAndText(elementText)
{
  var content = CreateDivWithClass("history-content");
  var contentHeader = CreateContentHeader(elementText.header);
  var contentText = CreateContentText(elementText.pArray);
  content.appendChild(contentHeader);
  content.appendChild(contentText);
  return content;
}

function CreateContentHeader(header)
{
  var contentHeader = CreateDivWithClass("history-content-header");
  contentHeader.appendChild(CreateH1WithText(header));
  return contentHeader;
}

function CreateContentText(pArray)
{
  var contentText = CreateDivWithClass("history-text");
  for (var j = 0; j < pArray.length; j++)
  {
    var p = CreatePWithText(pArray[j]);
    contentText.appendChild(p);
  }
  return contentText;
}


document.addEventListener("DOMContentLoaded", function()
{
  var text1 = new HobbyText
  (
    "My Music",
    [
      "I listen to everything I like...",
      "Okayyyy!!! For example, Linkin Park, Skillet,Король и Шут, Пикник,30 Seconds to Mars и много чего еще."
    ]
  );
  var text2 = new HobbyText
  (
    "My Films",
    [
      "I almost don't watch films!",
      "Wait, wait, wait!!! Однажды в Голливуде или Оно например. Предпочитаю ужастики."
    ]
  );

  var obj1 = new Hobby("img/music-bg.jpg", text1);
  var obj2 = new Hobby("img/film-bg.jpg", text2);

  HobbyRender(document.querySelector("main"), obj1, obj2);
})

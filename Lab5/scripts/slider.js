class Slide
{
  constructor(src, name)
  {
    this.src = "img/gallery/" + src;
    this.name = name;
  }

  GetHtmlCode()
  {
    var slideImg = CreateDivWithClass("slide");
    slideImg.style.backgroundImage = "url(" + this.src + ")";

    var textValue = document.createElement("h1");
    textValue.textContent = this.name;

    $(slideImg).append(textValue);

    var animatedSlide = $(slideImg).hide().fadeIn(700)

    return animatedSlide;
  }
}

function Slider(slides, parent)
{
  var slides = slides;
  var activeSlideInd = 0;
  this.parent = parent;

  this.GetSlides = () => slides;
  this.GetActiveSlideIndex = () => activeSlideInd;
  this.SetActiveSlideIndex = function(value)
  {
    var currentSlideInd = (value >= 0)
                          ? value % (slides.length)
                          : slides.length - Math.abs(value) % (slides.length);
    activeSlideInd = currentSlideInd;
  }

  this.Render = function()
  {
    var slider = GetHtmlCode(this);
    this.parent.append(slider);
  }

  function GetHtmlCode(obj)
  {
    var slider = CreateDivWithClass("slider");

    var currentSlide = GetActiveSlide(obj);
    $(slider).append(currentSlide.GetHtmlCode());

    var sliderStatement = GetSliderStatement(obj);
    $(slider).append(sliderStatement);

    return $(slider);
  }

  function GetActiveSlide(obj)
  {
    var slides = obj.GetSlides();
    var currentSlide = slides[obj.GetActiveSlideIndex()];
    return currentSlide;
  }

  function GetSliderStatement(obj)
  {
    var ind = obj.GetActiveSlideIndex();
    var maxInd = obj.GetSlides().length;

    var sliderStatement = CreateDivWithClass("sliderStatement");

    var arrowLeft = GetSliderArrow();
    var arrowRight = GetSliderArrow();

    BindForwardArrow(obj, arrowRight);
    BindBackwardArrow(obj, arrowLeft);

    var textValue = document.createElement("span");
    textValue.textContent = "Фото " + (parseInt(ind) + 1) + " из " + maxInd;

    var components = [arrowLeft, textValue, arrowRight];
    $(sliderStatement).append(components);

    return sliderStatement;
  }

  function GetSliderArrow()
  {
    var arrow = CreateDivWithClass("slideArrow");
    return $(arrow);
  }

  function BindForwardArrow(obj, arrow)
  {
    arrow.on("click", function()
    {
      var currentIndex = obj.GetActiveSlideIndex();
      obj.SetActiveSlideIndex(currentIndex + 1);
      Update(obj);
    })
  }

  function BindBackwardArrow(obj, arrow)
  {
    arrow.on("click", function()
    {
      var currentIndex = obj.GetActiveSlideIndex();
      obj.SetActiveSlideIndex(currentIndex - 1);
      Update(obj);
    })
  }

  function Update(obj)
  {
    SlideUpdate(obj)
    StatementUpdate(obj);
  }

  function SlideUpdate(obj)
  {
    var oldSlide = obj.parent.find(".slide");
    var newSlide = GetActiveSlide(obj);
    oldSlide.replaceWith(newSlide.GetHtmlCode());
  }

  function StatementUpdate(obj)
  {
    var oldStatement = obj.parent.find(".sliderStatement");
    var newStatement = GetSliderStatement(obj);
    oldStatement.replaceWith(newStatement);
  }

  this.Render();
}



// <div class="slider">
//   <div class="slide">
//     <h1>Test</h1>
//   </div>
//   <div class="sliderStatement">
//     <div class="slideArrow"></div>
//     <span>Фото 1/10</span>
//     <div class="slideArrow"></div>
//   </div>
// </div>

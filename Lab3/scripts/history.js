// Use in page ONLY width
// render.js!!

function CookieTableRender()
{
  var table = CreateEmptyTableWithHeader("История посещений за 10 дней");

  var currentRow;
  var currentValue;
  var currentName;

  var cookiesArr = GetDocumentCookies();

  currentRow = AddEmptyRowInTableBody(table);
  AddCellWithValueInTableInLastRow(table, "Страница");
  AddCellWithValueInTableInLastRow(table, "Количество посещений");

  for (var cookie in cookiesArr)
  {
    currentValue = cookiesArr[cookie].value;
    currentName = cookiesArr[cookie].name;

    currentRow = AddEmptyRowInTableBody(table);
    AddCellWithValueInTableInLastRow(table, currentName);
    AddCellWithValueInTableInLastRow(table, currentValue);
  }
  RenderTableOnContainer("#tableContainer", table);
}

function SessionStorageTableRender()
{
  var table = CreateEmptyTableWithHeader("История посещений за текущий сеанс");
  var currentPageValue;
  var currentRow;

  currentRow = AddEmptyRowInTableBody(table);
  AddCellWithValueInTableInLastRow(table, "Страница");
  AddCellWithValueInTableInLastRow(table, "Количество посещений");

  for (var pageName in sessionStorage)
  {
    if (pageName === "length") break;
    currentPageValue = sessionStorage[pageName];
    currentRow = AddEmptyRowInTableBody(table);
    AddCellWithValueInTableInLastRow(table, pageName);
    AddCellWithValueInTableInLastRow(table, currentPageValue);
  }
  RenderTableOnContainer("#tableContainer", table);
}

function GetDocumentCookies()
{
  var cookieSplit = document.cookie.split(";");
  var cookieInfoObjs = [];

  for (var i = 0; i < cookieSplit.length; i++)
  {
    var values = cookieSplit[i].split("=");
    var currentName = values[0].trim();
    var currentValue = values[1];
    cookieInfoObjs[i] = new CookieInfo(currentName, currentValue);
  }

  return cookieInfoObjs;
}

function CookieInfo(name, value)
{
  this.name = name;
  this.value = value;
}

function RenderTableOnContainer(containerSelector, table)
{
  var main = document.querySelector(containerSelector);
  main.appendChild(table);
}

document.addEventListener("DOMContentLoaded", function()
{
  CookieTableRender();
  SessionStorageTableRender()
});

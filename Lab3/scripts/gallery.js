// Use in page ONLY width
// render.js!!

function TableRender(titles, images) {
  var cell = document.getElementById("galleryTable").querySelectorAll("td");
  for (var i = 0; i < cell.length; i++)
  {
    var cellImage = cell[i].querySelector("img");
    cellImage.setAttribute("src", "img/gallery/" + images[i]);
    cellImage.setAttribute("title", titles[i]);
    cellImage.addEventListener("click", function(event)
    {
      var parent = document.getElementById("root");
      RenderZoomPhotoBox(parent, event.target.getAttribute("src"));
    });
    cell[i].querySelector("p").textContent = titles[i];
  }
}

function RenderZoomPhotoBox(parent, imgPath)
{
  var parent = document.getElementById("root");
  var box = new ZoomPhotoBox(parent, imgPath);
  box.Render();
}

function ZoomPhotoBox(parent, imgPath)
{
  this.root;
  if (parent != undefined)
  {
    var box = CreateFormBox();
    var header = CreateH1WithText("Your Photo");
    var bg = CreateDivWithClass("zoomPhotoBG");
    bg.style.backgroundImage = "url(" + imgPath + ")";
    box.appendChild(header);
    box.appendChild(bg);
    box.classList.add("zoomPhoto");
    this.root = box;
    parent.insertBefore(box, parent.firstChild);
  }

  this.Render = function()
  {
    if (this.root != undefined)
    {
      this.root.style.display = "block"
    }
  }
}

function ZoomPhoto(imgPath)
{
  var background = document.querySelector(".zoomPhoto .zoomPhotoBG");
  var parent = document.querySelector(".zoomPhoto");
  // background.setAttribute("src", imgPath);
  background.style.backgroundImage = "url(" + imgPath + ")";
  parent.style.display = "block";
}

document.addEventListener("DOMContentLoaded", function()
{
  var titles = [
    "Rotator 0",
    "Rotator 12",
    "Rotator 24",
    "Rotator 36",
    "Rotator 48",
    "Rotator 60",
    "Rotator 72",
    "Rotator 84",
    "Rotator 96",
    "Rotator 108",
    "Rotator 120",
    "Rotator 132",
    "Rotator 144",
    "Rotator 156",
    "Rotator 168"
  ];
  var images =
  [
    "gallery1.jpg",
    "gallery2.jpg",
    "gallery3.jpg",
    "gallery4.jpg",
    "gallery5.jpg",
    "gallery6.jpg",
    "gallery7.jpg",
    "gallery8.jpg",
    "gallery9.png",
    "gallery10.jpg",
    "gallery11.jpg",
    "gallery12.jpg",
    "gallery13.jpg",
    "gallery14.jpg",
    "MisterClass.jpg"
  ];
  TableRender(titles, images);
})

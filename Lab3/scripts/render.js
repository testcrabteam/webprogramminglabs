function CreateDiv()
{
  return document.createElement("div");
}

function CreateDivWithClass(className)
{
  var div = CreateDiv();
  div.className = className;
  return div;
}

function CreateH1WithText(text)
{
  var el = document.createElement("h1");
  el.textContent = text;
  return el;
}

function CreatePWithText(text)
{
  var el = document.createElement("p");
  el.textContent = text;
  return el;
}

function CreateUlWithNListsAndContentsWidthClasses(textContents, classes)
{
  var ul = CreateUlWithNListsAndContents(textContents);
  var elements = ul.childNodes;
  for (var i = 0; i < elements.length; i++)
  {
    if (classes[i] != undefined) AddClassToElement(elements[i], classes[i]);
  }
  return ul;
}

function CreateUlWithNListsAndContents(textContents)
{
  var ul = CreateUl();
  for (var i = 0; i < textContents.length; i++)
  {
    var li = CreateLiWithText(textContents[i]);
    ul.appendChild(li);
  }
  return ul;
}

function CreateUl()
{
  return document.createElement("ul");
}

function CreateLiWithText(text)
{
  var li = CreateLi();
  li.textContent = text;
  return li;
}

function CreateLi()
{
  return document.createElement("li");
}

function CreateSelectWithOptionsAndContents(textContents)
{
  var select = CreateSelect();
  for (var i = 0; i < textContents.length; i++)
  {
    var option = CreateOptionWithText(textContents[i]);
    option.value = i + 1;
    select.appendChild(option);
  }
  return select;
}

function CreateSelect()
{
  return document.createElement("select");
}

function CreateOptionWithText(text)
{
  var option = CreateOption();
  option.textContent = text;
  return option;
}

function CreateOption()
{
  return document.createElement("option");
}

function AddClassToElement(el, className)
{
  if (el != undefined) el.classList.add(className);
  return el;
}

function AddIdToElement(el, idName)
{
  if (el != undefined) el.setAttribute("id", idName);
  return el;
}

function AddNameToElement(el, name)
{
  if (el != undefined) el.setAttribute("name", name);
  return el;
}

function CreateFormBox()
{
  var box = CreateDivWithClass("formBox");
  var boxHeader = CreateDivWithClass("closeFormHeader");
  var boxCloseButton = CreateDivWithClass("closeFormButton");
  boxHeader.appendChild(boxCloseButton);
  box.appendChild(boxHeader);
  boxCloseButton.addEventListener("click", function()
  {
    var parent = box.parentNode;
    parent.removeChild(box);
  });
  return box;
}

function CreateEmptyTableWithHeader(headerValue)
{
  var table = CreateEmptyTable();
  var header = document.createElement("caption");
  header.textContent = headerValue;
  var tableBody = table.firstChild;
  table.insertBefore(header, tableBody);
  return table;
}

function CreateEmptyTable()
{
  var table = document.createElement("table");
  var tableBody = document.createElement("tbody");
  table.appendChild(tableBody);
  return table;
}

function AddEmptyRowInTableBody(table)
{
  if (table != undefined)
  {
    var tableBody = table.querySelector("tbody");
    var tableRow = document.createElement("tr");
    tableBody.appendChild(tableRow);
    return tableRow;
  }
  return table;
}

function AddCellWithValueInTableInLastRow(table, textValue)
{
  if (table != undefined)
  {
      var tableRow = table.querySelector("tr:last-child");
      if (tableRow == undefined) tableRow = AddEmptyRowInTableBody(table);
      var cell = document.createElement("td");
      cell.textContent = textValue;
      tableRow.appendChild(cell);
  }
}

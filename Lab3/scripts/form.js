  // Use in page ONLY width
// render.js and calendar.js!!!

function AddEventListeners()
{
  var inputs = document.querySelectorAll('input[type="text"], textarea');

  var fioInput = document.querySelector('input[name = "name"]');
  BindEventBlurOnInputWithFunc(fioInput, HandleNameInputFormatOnThreeWords)

  var phoneInput = document.querySelector('input[name = "phone"]');
  BindEventBlurOnInputWithFunc(phoneInput, HandlePhoneInputOnRusFormat);

  var textarea = document.querySelector('textarea[name="exampleSolution"]');
  BindEventBlurOnInputWithFunc(textarea, HandleTextareaInputOnFloatValue);

  var emailInput = document.querySelector('input[name = "email"]');
  BindEventBlurOnInputWithFunc(emailInput, HandleFormInputOnTextFilling);

  BindEventClickOnResetButton();
}

function BindEventBlurOnInputWithFunc(input, func)
{
  if (input != undefined)
  {
    input.addEventListener("blur", function(event)
    {
      func(input);
      var formName = input.getAttribute("form");
      var form = document.getElementById(formName);
      SetSubmitDisabledByFormReadiness(form);
    })
  }
}

function SetSubmitDisabledByFormReadiness(form)
{
  if (form != undefined)
  {
    var submit = form.querySelector('input[type="submit"]');
    if (IsFormReadyToSubmit(form)) submit.disabled = false;
    else
    {
      submit.disabled = true;
      console.log("Disable");
    }
  }
}

function IsFormReadyToSubmit(form)
{
  var inputs = form.querySelectorAll('input[type="text"]:not([disabled]), textarea');
  console.log(inputs);
  if (inputs != undefined)
  {
    for (var i = 0; i < inputs.length; i++)
    {
      if (ContainsCollectionElement(inputs[i].classList, "readyInput")) return true;
    }
  }
  return false;
}

function ContainsCollectionElement(list, element)
{
  if (list != undefined && element != undefined)
  {
    for (var i = 0; i < list.length; i++)
    {
      if (list[i] === element) return true;
    }
  }
  return false;
}

function HandleFormInputOnTextFilling(currentInput)
{
  if(!IsFormInputFilling(currentInput))
  {
    InputHTMLUpdateFail(currentInput);
    InputDescFailHTMLRender(currentInput, "Ошибка! Поле обязательно к заполнению!");
  }
  else MakeInputReadyToSubmit(currentInput);
}

function IsFormInputFilling(currentInput)
{
  if (currentInput.value === "") return false;
  return true;
}

function InputHTMLUpdateFail(currentInput)
{
  currentInput.classList.add("failedInput");
  currentInput.focus();
}

function InputDescFailHTMLRender(currentInput, desc)
{
  RemoveErrorMessageFromInput(currentInput);
  var errorText = CreatePWithText(desc);
  var parent = currentInput.parentNode;
  parent.insertBefore(errorText, currentInput.nextSibling);
}

function GetErrorMessageOnInput(currentInput)
{
  var inputName = currentInput.name;
  var errorMessage = document.querySelector('input[name = ' + inputName + ']' + '.failedInput + p');
  return errorMessage;
}

function RemoveErrorMessageFromInput(currentInput)
{
  var errorMessage = GetErrorMessageOnInput(currentInput);
  if (currentInput.nextSibling === errorMessage)
  {
    var parent = errorMessage.parentNode;
    parent.removeChild(errorMessage);
  }
}

function MakeInputReadyToSubmit(input)
{
  RemoveErrorMessageFromInput(input);
  input.classList.remove("failedInput");
  input.classList.add("readyInput");
}

function HandleNameInputFormatOnThreeWords(input)
{
  var formId = input.getAttribute("form");
  var formName = input.getAttribute("name");
  var obj = new FormObj(formId, formName);
  var reg = /\w*\s\w*\s\w*/;  //"Begin content end"
  var errorText = "Ошибка! Введите 3 слова, разделенных одним пробелом!";
  HandleFormInputFormatOnRegularExpression(obj, reg, errorText);
}

function HandlePhoneInputOnRusFormat(input)
{
  var formId = input.getAttribute("form");
  var formName = input.getAttribute("name");
  var obj = new FormObj(formId, formName);
  var reg = /^\+(7|3)\d{9,11}$/; //+7XXXXXXXXX(+xx) or +3XXXXXXXXX(+xx)
  var errorText = "Ошибка! Формат ввода: +7XXXXXXXXX(xx) или +3XXXXXXXXX(xx)";
  HandleFormInputFormatOnRegularExpression(obj, reg, errorText);
}

function HandleFormInputFormatOnRegularExpression(formObj, reg, errorText)
{
  var form = document.querySelector("#" + formObj.formId);
  var selector = (formObj.textAreaName == undefined)
  ? 'input[name = ' + formObj.inputName + ']'
  : 'textarea[name = ' + formObj.textAreaName + ']';
  var input = form.querySelector(selector);
  if (form != undefined && input != undefined)
    HandleInputOnRegularExpression(input, reg, errorText)
}

function HandleTextareaInputOnFloatValue(formId)
{
  var obj = new FormObj(formId, "name", "exampleSolution");
  var reg = /\d\.\d/;  //"xxx.xxx - float value"
  var errorText = "Ошибка! Введите минимум одно вещественное число!";
  HandleFormInputFormatOnRegularExpression(obj, reg, errorText);
}

function FormObj(id, inputName, textAreaName = null)
{
  this.formId = id;
  this.inputName = inputName;
  this.textAreaName = textAreaName;
}

function HandleInputOnRegularExpression(input, regFormat, errorText)
{
  if (!regFormat.test(input.value))
  {
    InputHTMLUpdateFail(input);
    InputDescFailHTMLRender(input, errorText);
  }
  else MakeInputReadyToSubmit(input);
}

function BindEventClickOnResetButton()
{
  document.querySelector('input.helper-button[type = "reset"]')
  .addEventListener("click", function(event)
  {
    var formId = event.currentTarget.parentNode
                .getAttribute("id");
    ResetFormInputs(formId);
  });
}

function ResetFormInputs(formId)
{
  var form = document.getElementById(formId);
  var inputsSelector =
  'input[type = "text"], input[type = "password"], textarea';
  var inputs = form.querySelectorAll(inputsSelector);
  for (var i = 0; i < inputs.length; i++) ResetFormInput(inputs[i]);
}

function ResetFormInput(input)
{
  input.value = "";
  input.classList.remove("readyInput");
}

document.addEventListener("DOMContentLoaded", function()
{
  AddEventListeners();
  var calendarParent = document.querySelector("#form .inputBlock:nth-child(4)");
  var calendar = new HtmlCalendar(calendarParent);
  var dateInput = document.querySelector('#form input[name = "date"]');
  var formCalendar = new FormHtmlCalendar(calendar, dateInput);
  formCalendar.Render();
});

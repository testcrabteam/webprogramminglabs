// Use in page ONLY width
// render.js!!!


function HandleFormInputsOnTextFilling(formId)
{
  var form = document.querySelector("#" + formId);
  var inputs = form.querySelectorAll('input[type = "text"], textarea');
  CheckFormInputs(inputs);
}

function HandleNameInputFormatOnThreeWords(formId)
{
  var obj = new FormObj(formId, "name");
  var reg = /\w*\s\w*\s\w*/;  //"Begin content end"
  var errorText = "Ошибка! Введите 3 слова, разделенных одним пробелом!";
  HandleFormInputFormatOnRegularExpression(obj, reg, errorText);
}

function HandlePhoneInputOnRusFormat(formId)
{
  var obj = new FormObj(formId, "phone");
  var reg = /^\+(7|3)\d{9,11}$/; //+7XXXXXXXXX(+xx) or +3XXXXXXXXX(+xx)
  var errorText = "Ошибка! Формат ввода: +7XXXXXXXXX(xx) или +3XXXXXXXXX(xx)";
  HandleFormInputFormatOnRegularExpression(obj, reg, errorText);
}

function HandleFormInputFormatOnRegularExpression(formObj, reg, errorText)
{
  var form = document.querySelector("#" + formObj.formId);
  var selector = (formObj.textAreaName == undefined)
  ? 'input[name = ' + formObj.inputName + ']'
  : 'textarea[name = ' + formObj.textAreaName + ']';
  var input = form.querySelector(selector);
  if (form != undefined && input != undefined)
    HandleInputOnRegularExpression(input, reg, errorText)
}

function HandleTextareaInputOnFloatValue(formId)
{
  var obj = new FormObj(formId, "name", "exampleSolution");
  var reg = /\d\.\d/;  //"xxx.xxx - float value"
  var errorText = "Ошибка! Введите минимум одно вещественное число!";
  HandleFormInputFormatOnRegularExpression(obj, reg, errorText);
}

function FormObj(id, inputName, textAreaName = null)
{
  this.formId = id;
  this.inputName = inputName;
  this.textAreaName = textAreaName;
}

function HandleInputOnRegularExpression(input, regFormat, errorText)
{
  if (!regFormat.test(input.value))
  {
    InputHTMLUpdateFail(input);
    InputDescFailHTMLRender(input, errorText);
  }
}

function CheckFormInputs(inputs)
{
  for (var i = 0; i < inputs.length; i++)
  {
    var currentInput = inputs[i];
    if(IsFormInputFilling(currentInput))
    {
      InputHTMLUpdateFail(currentInput);
      InputDescFailHTMLRender(currentInput, "Ошибка! Поле обязательно к заполнению!");
      break;
    }
  }
}

function IsFormInputFilling(currentInput)
{
  if (currentInput.value === "") return true;
  return false;
}

function InputHTMLUpdateFail(currentInput)
{
  currentInput.classList.add("failedInput");
  currentInput.focus();
}

function InputDescFailHTMLRender(currentInput, desc)
{
  if (currentInput.nextSibling !== document.querySelector(".failedInput + p"))
  {
    var errorText = CreatePWithText(desc);
    var parent = currentInput.parentNode;
    parent.insertBefore(errorText, currentInput.nextSibling);
  }
}

function RemoveErrorsTextFromPage(formId)
{
  var form = document.querySelector("#" + formId);
  var failedTexts = form.querySelectorAll(".failedInput + p");
  var failedInputs = form.querySelectorAll(".failedInput");
  for (var i = 0; i < failedInputs.length; i++)
  {
    failedInputs[i].classList.remove("failedInput");
    var parent = failedTexts[i].parentNode;
    parent.removeChild(failedTexts[i]);
  }
}


document.addEventListener("DOMContentLoaded", function()
{
  document.querySelector('input.helper-button[type = "submit"]')
  .addEventListener("click", function(event)
  {
    var formId = event.currentTarget.getAttribute("form");
    HandleFormInputsOnTextFilling(formId);
    HandleNameInputFormatOnThreeWords(formId);
    HandlePhoneInputOnRusFormat(formId);
    HandleTextareaInputOnFloatValue(formId);
  });
  document.querySelector('input.helper-button[type = "reset"]')
  .addEventListener("click", function(event)
  {
    var formId = event.currentTarget.parentNode.getAttribute("id");
    RemoveErrorsTextFromPage(formId);
  })
});

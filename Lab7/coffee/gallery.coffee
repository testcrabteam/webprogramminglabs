TableRender = (titles, img) ->
  cell = document.getElementById('galleryTable').querySelectorAll('td')
  i = 0
  while i < cell.length
    cell[i].querySelector('img').setAttribute 'src', 'img/' + img[0]
    cell[i].querySelector('img').setAttribute 'title', titles[i]
    cell[i].querySelector('img').style.transform = 'rotateZ(' + i * 12 + 'deg)'
    cell[i].querySelector('p').textContent = titles[i]
    i++
  return

document.addEventListener 'DOMContentLoaded', ->
  titles = [
    'Rotator 0'
    'Rotator 12'
    'Rotator 24'
    'Rotator 36'
    'Rotator 48'
    'Rotator 60'
    'Rotator 72'
    'Rotator 84'
    'Rotator 96'
    'Rotator 108'
    'Rotator 120'
    'Rotator 132'
    'Rotator 144'
    'Rotator 156'
    'Rotator 168'
  ]
  path = [ 'gallery.jpg' ]
  TableRender titles, path
  return

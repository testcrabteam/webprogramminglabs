CreateDiv = ->
  document.createElement 'div'

CreateDivWithClass = (className) ->
  div = CreateDiv()
  div.className = className
  div

CreateH1WithText = (text) ->
  el = document.createElement('h1')
  el.textContent = text
  el

CreatePWithText = (text) ->
  el = document.createElement('p')
  el.textContent = text
  el

# Use in page ONLY width
# render.js!!!

HandleFormInputsOnTextFilling = (formId) ->
  form = document.querySelector('#' + formId)
  inputs = form.querySelectorAll('input[type = "text"], textarea')
  CheckFormInputs inputs
  return

HandleNameInputFormatOnThreeWords = (formId) ->
  obj = new FormObj(formId, 'name')
  reg = /\w*\s\w*\s\w*/
  #"Begin content end"
  errorText = 'Ошибка! Введите 3 слова, разделенных одним пробелом!'
  HandleFormInputFormatOnRegularExpression obj, reg, errorText
  return

HandlePhoneInputOnRusFormat = (formId) ->
  obj = new FormObj(formId, 'phone')
  reg = /^\+(7|3)\d{9,11}$/
  #+7XXXXXXXXX(+xx) or +3XXXXXXXXX(+xx)
  errorText = 'Ошибка! Формат ввода: +7XXXXXXXXX(xx) или +3XXXXXXXXX(xx)'
  HandleFormInputFormatOnRegularExpression obj, reg, errorText
  return

HandleFormInputFormatOnRegularExpression = (formObj, reg, errorText) ->
  form = document.querySelector('#' + formObj.formId)
  selector = if formObj.textAreaName == undefined then 'input[name = ' + formObj.inputName + ']' else 'textarea[name = ' + formObj.textAreaName + ']'
  input = form.querySelector(selector)
  if form != undefined and input != undefined
    HandleInputOnRegularExpression input, reg, errorText
  return

HandleTextareaInputOnFloatValue = (formId) ->
  obj = new FormObj(formId, 'name', 'exampleSolution')
  reg = /\d\.\d/
  #"xxx.xxx - float value"
  errorText = 'Ошибка! Введите минимум одно вещественное число!'
  HandleFormInputFormatOnRegularExpression obj, reg, errorText
  return

FormObj = (id, inputName, textAreaName = null) ->
  @formId = id
  @inputName = inputName
  @textAreaName = textAreaName
  return

HandleInputOnRegularExpression = (input, regFormat, errorText) ->
  if !regFormat.test(input.value)
    InputHTMLUpdateFail input
    InputDescFailHTMLRender input, errorText
  return

CheckFormInputs = (inputs) ->
  i = 0
  while i < inputs.length
    currentInput = inputs[i]
    if IsFormInputFilling(currentInput)
      InputHTMLUpdateFail currentInput
      InputDescFailHTMLRender currentInput, 'Ошибка! Поле обязательно к заполнению!'
      break
    i++
  return

IsFormInputFilling = (currentInput) ->
  if currentInput.value == ''
    return true
  false

InputHTMLUpdateFail = (currentInput) ->
  currentInput.classList.add 'failedInput'
  currentInput.focus()
  return

InputDescFailHTMLRender = (currentInput, desc) ->
  if currentInput.nextSibling != document.querySelector('.failedInput + p')
    errorText = CreatePWithText(desc)
    parent = currentInput.parentNode
    parent.insertBefore errorText, currentInput.nextSibling
  return

RemoveErrorsTextFromPage = (formId) ->
  form = document.querySelector('#' + formId)
  failedTexts = form.querySelectorAll('.failedInput + p')
  failedInputs = form.querySelectorAll('.failedInput')
  i = 0
  while i < failedInputs.length
    failedInputs[i].classList.remove 'failedInput'
    parent = failedTexts[i].parentNode
    parent.removeChild failedTexts[i]
    i++
  return

document.addEventListener 'DOMContentLoaded', ->
  document.querySelector('input.helper-button[type = "submit"]').addEventListener 'click', (event) ->
    formId = event.currentTarget.getAttribute('form')
    HandleFormInputsOnTextFilling formId
    HandleNameInputFormatOnThreeWords formId
    HandlePhoneInputOnRusFormat formId
    HandleTextareaInputOnFloatValue formId
    return
  document.querySelector('input.helper-button[type = "reset"]').addEventListener 'click', (event) ->
    formId = event.currentTarget.parentNode.getAttribute('id')
    RemoveErrorsTextFromPage formId
    return
  return

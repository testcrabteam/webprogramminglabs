function CreateDiv()
{
  return document.createElement("div");
}

function CreateDivWithClass(className)
{
  var div = CreateDiv();
  div.className = className;
  return div;
}

function CreateH1WithText(text)
{
  var el = document.createElement("h1");
  el.textContent = text;
  return el;
}

function CreatePWithText(text)
{
  var el = document.createElement("p");
  el.textContent = text;
  return el;
}

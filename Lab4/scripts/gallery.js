// Use in page ONLY width
// render.js!!

document.addEventListener("DOMContentLoaded", function()
{
  var slides =
  [
    new Slide("gallery1.jpg", "Когда-то..."),
    new Slide("gallery2.jpg", "...давным..."),
    new Slide("gallery3.jpg", "...давно..."),
    new Slide("gallery4.jpg", "...я сказал себе..."),
    new Slide("gallery5.jpg", "...есть..."),
    new Slide("gallery6.jpg", "...только ты..."),
    new Slide("gallery7.jpg", "...сам себе хозяин..."),
    new Slide("gallery8.jpg", "...и лучший друг..."),
    new Slide("gallery9.png", "...иди вперед..."),
    new Slide("gallery10.jpg", "...не падай духом..."),
    new Slide("gallery11.jpg", "...всегда смотри вперед..."),
    new Slide("gallery12.jpg", "...живи одним моментом..."),
    new Slide("gallery13.jpg", "...цени свою жизнь..."),
    new Slide("gallery14.jpg", "...будь счастлив..."),
    new Slide("MisterClass.jpg", "..и да пребудет с тобой Сила!"),
  ];
  var parent = $("#sliderContainer");
  var slider = new Slider(slides, parent);
})
